package com.qa.testng;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeClass;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;


public class TestCase1 {

	// ******* Declaraci�n de variables *******
	WebDriver driver;
	int index = 0;
	
	// Cookies
	By cookies = By.id("accept-choices");
	
	By ResertDB = By.id("restoreDBBtn");
	// Caja de texto
	By Text = By.cssSelector(".cm-m-sql");
	
	//Bot�n
	By button = By.cssSelector(".ws-btn");
	
	// Tabla BD
	By yourDB= By.id("yourDB");
	String nameDB= "TablaJuanPerez";
	
	// Tabla resultados
	By DivResult = By.id("divResultSQL");
	
	
	// Repetir alguna prueba fallida
	boolean Failtest3 = false;
	boolean Failtest4 = false;
	boolean Failtest5 = false;
	boolean Failtest6 = false;
	boolean Failtest7 = false;
	
	
	
	
@BeforeClass
  public void beforeClass() {
	System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
	driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://www.w3schools.com/sql/trysql.asp?filename=trysql_select_distinct");
			

  }

	
	
@Test
 
public void main() {
	
	//Tiempo de espera
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.presenceOfElementLocated(cookies));

	//Aceptar cookies
	WebElement searchCookies = driver.findElement(cookies);
	searchCookies.click();
	
	System.out.println("*******CREAMOS LOS WEBELEMENT*********");
	//Creamos WebElement
	WebElement searchText = driver.findElement(Text);
	WebElement searchButton = driver.findElement(button);	
	
	
	
	// RestaurarBD
	ResetDB();
	// Punto 3 - Crear tabla
	createTable(searchText,searchButton);
	
	// Punto 4 - Seleccionar Customers - Spain
	//CustomersSpain(searchText,searchButton);
	
	//Punto 5 -Notas Empleados 1925-1390
	//AnOldChum(searchText,searchButton);
	
	// Punto 6 - Comprobar pedido materes Longlife Tofu
	//LonglifeTofu (searchText,searchButton);
	
	// RestaurarBD
	//ResetDB();
	
	//Punto 7 - Pedidos 30-12-1997 / ID 4
	//employeeID4(searchText,searchButton);
	
	//Repetir pruebas fallidas
	//RepeatFail(Failtest3,Failtest4,Failtest5,Failtest6,Failtest7,searchText,searchButton);
	
	
	
}

public void ResetDB() {
	// Reseteamos BD
	WebElement searchResert =driver.findElement(ResertDB);
	System.out.println("++++++++++++++++++VAMOS A RESETEAR BD+++++++++++++");
	searchResert.click();
	Alert alert = driver.switchTo().alert();
	alert.accept();
	WebDriverWait wait = new WebDriverWait(driver, 10);
	
	
	
}
//------------------------------------------------------------------------------------
// ---------------------  Punto 3 - Crear tabla ---------------------
//------------------------------------------------------------------------------------
  public void createTable(WebElement searchText, WebElement searchButton) {
	
	System.out.println("++++++++++++++++++PUBLIC CreateTable+++++++++++++");
	  
	 
	// Eliminar el texto inicial en la web.
	searchText.clear();
	System.out.println("Limpiamos");
	//Introducimos la consulta SQL
	searchText.sendKeys("CREATE TABLE "+nameDB+" (Nombre char(20),Apellido1 char(20),Apellido2 char(20),Address char(150),City char(70),Country char(75));");
	
	//Hacer click en el bot�n
	searchButton.click();
	
	//Tiempo de espera
	WebDriverWait wait = new WebDriverWait(driver, 3);
	
	
	
	//Verificar si se cre� la nueva tabla
	WebElement seachDB = driver.findElement(yourDB);
	String textDb = seachDB.findElement(By.xpath("//table/tbody/tr[last()]/td[last()]")).getText();
	System.out.println("Tu DB es:"+textDb);
	
	//Repetir prueba si falla
	if(!textDb.equals(nameDB))
		Failtest3 = true;
	
	assertTrue(textDb.equals(nameDB),"+++ No se cre� correctamente la tabla +++");
	
	
	
	
  }
 
//------------------------------------------------------------------------------------
//--------------------- Punto 4 - Seleccionar Customers - Spain ---------------------
//------------------------------------------------------------------------------------
  
  
  public void CustomersSpain(WebElement searchText, WebElement searchButton) {
	// Eliminar el texto inicial en la web.
	searchText.clear();
	
	//Introducimos la consulta SQL
	searchText.sendKeys("SELECT count(*) FROM Customers WHERE Country=\"Spain\"");
	
	//Hacer click en el bot�n
	searchButton.click();
	
	//Verificar resultado
	WebElement seachDivResult = driver.findElement(DivResult);
	String textResult = seachDivResult.findElement(By.xpath("//div/table/tr[last()]/td[last()]")).getText();
	
	//Repetir prueba si falla
	if(!textResult.equals("5"))
		Failtest4 = true;
	
	
	assertTrue(textResult.equals("5"),"+++ Error Resultado: CustomersSpain +++");
	
	  
  }

// ------------------------------------------------------------------------------------
//--------------------- Punto 5 -Notas Empleados 1925-1390 ---------------------
//------------------------------------------------------------------------------------
  public void  AnOldChum(WebElement searchText, WebElement searchButton) {
	// Eliminar el texto inicial en la web.
		searchText.clear();
	//Introducimos la consulta SQL
		searchText.sendKeys("SELECT * FROM Employees WHERE BirthDate BETWEEN '1925-01-01' AND '1930-12-12'");
	
	//Hacer click en el bot�n
		searchButton.click();
		
	//Verificar resultado
		WebElement seachDivResult = driver.findElement(DivResult);
		String textResult = seachDivResult.findElement(By.xpath("//div/table/tr[last()]/td[last()]")).getText();
		
		
	//Repetir prueba si falla
		if(!textResult.equals("An old chum."))
			Failtest5 = true;
		
		
		assertTrue(textResult.equals("An old chum."),"+++ Error Resultado: CustomersSpain +++");		
		
		
		
  }
  

//------------------------------------------------------------------------------------
//--------------------- Punto 6 - Comprobar pedido materes Longlife Tofu -----------
//------------------------------------------------------------------------------------
  public void  LonglifeTofu(WebElement searchText, WebElement searchButton) {
	// Eliminar el texto inicial en la web.
		searchText.clear();
	//Introducimos la consulta SQL
		searchText.sendKeys("SELECT p.ProductName as Producto, MAX(o.Quantity) as Cantidad FROM OrderDetails as o, Products as p WHERE o.ProductID = p.ProductID");
		
	//Hacer click en el bot�n
		searchButton.click();
				
	//Verificar resultado
		WebElement seachDivResult = driver.findElement(DivResult);
		String textResult = seachDivResult.findElement(By.xpath("//div/table/tr[last()]/td")).getText();
		
		//Repetir prueba si falla
		if(!textResult.equals("P�t� chinois"))
		Failtest6 = true;
		
		
		assertTrue(textResult.equals("P�t� chinois"),"+++ Error Resultado: CustomersSpain +++");		
	  
  }
  
  
//------------------------------------------------------------------------------------
//--------------------- Punto 7 - Pedidos 30-12-1997 / ID 4 -----------
//------------------------------------------------------------------------------------
 public void employeeID4(WebElement searchText,WebElement searchButton) {
	// Eliminar el texto inicial en la web.
			searchText.clear();
		//Introducimos la consulta SQL
			searchText.sendKeys("SELECT * FROM Orders as o WHERE o.OrderDate = '1997-01-30'");
			
		//Hacer click en el bot�n
			searchButton.click();
					
		//Verificar resultado
			WebElement seachDivResult = driver.findElement(DivResult);
			
			//for (var i=1; i < numCommands; ++i) {
				
			//}
			
			//String textResult = seachDivResult.findElement(By.xpath("//div/table/tr[last()]/td")).getText();
					
			//assertTrue(textResult.equals("P�t� chinois"),"+++ Error Resultado: CustomersSpain +++");
 }
 
//------------------------------------------------------------------------------------
//--------------------- Repetir pruebas fallidas -----------
//------------------------------------------------------------------------------------ 
 
 public void RepeatFail(boolean Failtest3,boolean Failtest4,boolean Failtest5,boolean Failtest6,boolean Failtest7,WebElement searchText,WebElement searchButton) {
	 
    // Punto 3 - Crear tabla
	 if (Failtest3) {	
		ResetDB();
		createTable(searchText,searchButton);
	 }
	 		
	// Punto 4 - Seleccionar Customers - Spain
	 if (Failtest4)
	 	CustomersSpain(searchText,searchButton);
			
	//Punto 5 -Notas Empleados 1925-1390
	 if (Failtest5)
		AnOldChum(searchText,searchButton);
			
	// Punto 6 - Comprobar pedido materes Longlife Tofu
	 if (Failtest6)
		LonglifeTofu (searchText,searchButton);

	//Punto 7 - Pedidos 30-12-1997 / ID 4
	 if (Failtest7) {
		// RestaurarBD
		ResetDB();
			
		//Punto 7 - Pedidos 30-12-1997 / ID 4
		employeeID4(searchText,searchButton);
	 }
	 	
	 
 }

@AfterClass
public void afterClass() {
	//driver.close();
}


}
